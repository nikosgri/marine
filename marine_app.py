import RPi.GPIO as GPIO
import time
import json

#Define GPIO input pins
meter_1_input_pin = 1  #GPIO01, pin#=28
meter_2_input_pin = 4  #GPIO04, pin#=07
meter_3_input_pin = 15 #GPIO15, pin#=10
meter_4_input_pin = 14 #GPIO14, pin#=08
water_1_input_pin = 12 #GPIO32, pin#=32
water_2_input_pin = 13 #GPIO33, pin#=33

#Define GPIO output pins
priza_1_output_pin = 20 #GPIO20, pin#=38
priza_2_output_pin = 21 #GPIO21, pin#=40
priza_3_output_pin = 22 #GPIO22, pin#=15
priza_4_output_pin = 23 #GPIO23, pin#=16
water_1_output_pin = 24 #GPIO24, pin#=18
water_2_output_pin = 25 #GPIO25, pin#=22

#Define flags
priza_1_status = 0 #electricity1, left red 3-PH
priza_2_status = 0 #electricity2, right red 3-PH
priza_3_status = 0 #electricity3, left blue 1-PH
priza_4_status = 0 #electricity4, right blue 1-PH
water_1_status = 0 #water1
water_2_status = 0 #water2

#Define counters
left_red_open_counter = 0   #Used to notify that the 3-PH left red socket opened
right_red_open_counter = 0  #Used to notify that the 3-PH right red socket opened
left_blue_open_counter = 0  #Used to notify that the 1-PH left blue socket opened
right_blue_open_counter = 0 #Used to notify that the 1-PH right blue socket opened
water_1_open_counter = 0    #Used to notify that the water 1 socket opened
water_2_open_counter = 0    #Used to notify that the water 2 socket opened
    
left_red_read_counter = 0       #Counting pulses from left red 3-PH
right_red_read_counter = 0      #Counting pulses from right red 3-PH
left_blue_read_counter = 0      #Counting pulses from left blue 1-PH 
right_blue_read_counter = 0     #Counting pulses from right blue 1-PH
left_water_1_read_counter = 0   #Counting pulses from left water valve 
right_water_2_read_counter = 0  #Counting pulses from right water valve

#Initialize GPIO pins INPUT/OUTPUT
def gpio_init():
    
    #GPIO.setmode(GPIO.BCM)
    
    #Input pins, used for reading pulses from metrics
    GPIO.setup(meter_1_input_pin, GPIO.IN) #Left red socket input
    GPIO.setup(meter_2_input_pin, GPIO.IN) #Right red socket input
    GPIO.setup(meter_3_input_pin, GPIO.IN) #Left blue socket input
    GPIO.setup(meter_4_input_pin, GPIO.IN) #Right blue socket input
    GPIO.setup(water_1_input_pin, GPIO.IN) #Left valve 
    GPIO.setup(water_2_input_pin, GPIO.IN) #Right valve
    
    #Output pins, used to open/close relay ports
    GPIO.setup(priza_1_output_pin, GPIO.OUT) #Left red socket
    GPIO.setup(priza_2_output_pin, GPIO.OUT) #Right red socket
    GPIO.setup(priza_3_output_pin, GPIO.OUT) #Left blue socket
    GPIO.setup(priza_4_output_pin, GPIO.OUT) #Right blue socket
    GPIO.setup(water_1_output_pin, GPIO.OUT) #Left valve 
    GPIO.setup(water_2_output_pin, GPIO.OUT) #Right valve
    
    #Notify user
    print("All GPIO pins (In/Out) initialized successfully")
    
#Handle opening and closing of marine's ports. OPEN = "1", CLOSE = "0"
def handle_ports(port_name, operation):
    
    #Current status of each port. All first declared as closed "0", in their idle state.
    global priza_1_status 
    global priza_2_status
    global priza_3_status
    global priza_4_status
    global water_1_status
    global water_2_status
    
    if port_name == "electricity1" and operation == 1:
        if priza_1_status != operation: #If current state of port is in close mode and the operation is 1, which means open port 
            open_left_red(port_name) 	#Call the function to open the appropriate port
            priza_1_status = operation	#Change the current state to open "1"
        else:
            read_left_red(port_name)	#If the state that the main sends you is "1" and you already there then read values only and update the file
            
    elif port_name == "electricity1" and operation == 0:  
        if priza_1_status != operation:    #If current state of port is in close mode and the operation is 0, which means close port
            close_left_red(port_name)      #Call the function to open the appropriate port
            priza_1_status = operation     #Change the current state to close "0"
        else:
            update_json_file(port_name, 0) #If the state that the main sends you is "0" and you already there, then update the file with 0 value.
            
    elif port_name == "electricity2" and operation == 1:
        if priza_2_status != operation:   #If current state of port is in close mode and the operation is 1, which means open port
            open_right_red(port_name)     #Call the function to open the appropriate port
            priza_2_status = operation    #Change the current state to open "1"
        else:
            read_right_red(port_name)     #If the state that the main sends you is "1" and you already there then read values only and update the file
            
    elif port_name == "electricity2" and operation == 0:
        if priza_2_status != operation:    #If current state of port is in close mode and the operation is 0, which means close port
            close_right_red(port_name)     #Call the function to open the appropriate port
            priza_2_status = operation     #Change the current state to close "0"
        else:
            update_json_file(port_name, 0) #If the state that the main sends you is "0" and you already there, then update the file with 0 value.
            
    elif port_name == "electricity3"  and operation == 1:
        if priza_3_status != operation:   #If current state of port is in close mode and the operation is 1, which means open port
            open_left_blue(port_name)     #Call the function to open the appropriate port
            priza_3_status = operation    #Change the current state to open "1"
        else:
            read_left_blue(port_name)     #If the state that the main sends you is "1" and you already there then read values only and update the file
            
    elif port_name == "electricity3" and operation == 0:
        if priza_3_status != operation:    #If current state of port is in close mode and the operation is 0, which means close port
            close_left_blue(port_name)     #Call the function to open the appropriate port
            priza_3_status = operation     #Change the current state to close "0"
        else:
            update_json_file(port_name, 0) #If the state that the main sends you is "0" and you already there, then update the file with 0 value.
            
    elif port_name == "electricity4"  and operation == 1:
        if priza_4_status != operation:   #If current state of port is in close mode and the operation is 1, which means open port
            open_right_blue(port_name)    #Call the function to open the appropriate port
            priza_4_status = operation    #Change the current state to open "1"
        else:
            read_right_blue(port_name)    #If the state that the main sends you is "1" and you already there then read values only and update the file
            
    elif port_name == "electricity4" and operation  == 0:
        if priza_4_status != operation:    #If current state of port is in close mode and the operation is 0, which means close port
            close_right_blue(port_name)    #Call the function to open the appropriate port
            priza_4_status = operation     #Change the current state to close "0"
        else:
            update_json_file(port_name, 0) #If the state that the main sends you is "0" and you already there, then update the file with 0 value.
            
    elif port_name == "water1" and operation == 1:
        if water_1_status != operation:   #If current state of port is in close mode and the operation is 1, which means open port
            open_water_1(port_name)       #Call the function to open the appropriate port
            water_1_status = operation    #Change the current state to open "1"
        else:
            read_water_1(port_name)       #If the state that the main sends you is "1" and you already there then read values only and update the file
            
    elif port_name == "water1" and operation == 0:
        if water_1_status != operation:    #If current state of port is in close mode and the operation is 0, which means close port
            close_water_1(port_name)       #Call the function to open the appropriate port
            water_1_status = operation     #Change the current state to close "0"
        else:
        	update_json_file(port_name, 0) #If the state that the main sends you is "0" and you already there, then update the file with 0 value.
        	
    elif port_name == "water2" and operation == 1:
        if water_2_status != operation:   #If current state of port is in close mode and the operation is 1, which means open port
            open_water_2(port_name)       #Call the function to open the appropriate port
            water_2_status = operation    #Change the current state to open "1"
        else:
            read_water_2(port_name)       #If the state that the main sends you is "1" and you already there then read values only and update the file
            
    elif port_name == "water2" and operation == 0:
        if water_2_status != operation:    #If current state of port is in close mode and the operation is 0, which means close port
            close_water_2(port_name)       #Call the function to open the appropriate port
            water_2_status = operation     #Change the current state to close "0"
        else:
            update_json_file(port_name, 0) #If the state that the main sends you is "0" and you already there, then update the file with 0 value.

########	Functions that opening sockets		########

#Left 3-PH
def open_left_red(port_name):
	
    global left_red_open_counter
    GPIO.output(priza_1_output_pin, GPIO.LOW) #Close relay port
    #Notify user
    print("Electricity 1 left red 3-PH opened") 
    print("Start reading")
    left_red_open_counter = left_red_open_counter + 1 #Count how many times left red 3-PH was opened
	read_left_red(port_name) #Open port in order to start reading pulses

#Right 3-PH
def open_right_red(port_name):
	
    global right_red_open_counter 
    GPIO.output(priza_2_output_pin, GPIO.LOW) #Close relay port
    #Notify user
    print("Electricity 2 right red 3-PH is opened")
    print("Start reading")
    right_red_open_counter = right_red_open_counter + 1 #Count how many times right red 3-PH was opened
	read_right_red(port_name) #Open port in order to start reading pulses

#Left 1-PH
def open_left_blue(port_name):
	
    global left_blue_open_counter 
    GPIO.output(priza_3_output_pin, GPIO.LOW) #Close relay port
    #Notify user
    print("Electricity 3 left blue 1-PH is opened")
    print("Start reading")
    left_blue_open_counter =  left_blue_open_counter + 1 #Count how many times left blue 1-PH was opened
	read_left_blue(port_name) #Open port in order to start reading pulses
    
#Right 1-PH
def open_right_blue(port_name):
	
    global right_blue_open_counter 
    GPIO.output(priza_4_output_pin, GPIO.LOW) #Close relay port
    #Notify user
    print("Electricity 4 left blue 1-PH is opened")
    print("Start reading")
    right_blue_open_counter =  right_blue_open_counter + 1 #Count how many times right blue 1-PH was opened
	read_right_blue(port_name) #Open port in order to start reading pulses

#Left water valve
def open_water_1(port_name):
	
    global water_1_open_counter 
    GPIO.output(water_1_output_pin, GPIO.LOW) #Close relay port
    #Notify user
    print("Water 1, at left  is opened")
    print("Start reading")
    water_1_open_counter =  water_1_open_counter + 1 #Count how many times left valve was opened
	read_water_1(port_name) #Open port in order to start reading pulses
 
#Right water valve 
def open_water_2(port_name):
	
    global water_2_open_counter 
    GPIO.output(water_2_output_pin, GPIO.LOW) #Close relay port
    #Notify user
    print("Water 2, at right  is opened")
    print("Start reading")
    water_2_open_counter =  water_2_open_counter + 1 #Count how many times right valve was opened
	read_water_2(port_name) #Open port in order to start reading pulses
    
########	END OF FUNCTIONS THAT OPENING SOCKETS	########

    
########	READING OPERATIONS	########
#Functions that operate reading kwh for each port.
def read_left_red(port_name):
    global left_red_read_counter #Global counter that counts high pulses from metrics
    red_left_value = GPIO.input(meter_1_input_pin) #Read input pin
    if red_left_value == GPIO.HIGH: #If a high pulse occurs
        left_red_read_counter = left_red_read_counter + 1 #Update the counter
        time.sleep(0.01) #Small delay to catch next pulse
    kwh = left_red_read_counter / 1000.0 #Real time measuring kwh
    update_json_file(port_name, kwh) #Update the value to the UI
    
    #Just for debugging purposes
    
    ##left_red_read_counter += 1
    ##kwh = left_red_read_counter / 1000.0
    ##update_json_file(port_name, kwh)
    
def read_right_red(port_name):
    global right_red_read_counter #Global counter that counts high pulses from metrics
    red_right_value = GPIO.input(meter_2_input_pin) #Read input pin
    if red_right_value == GPIO.HIGH: #If a high pulse occurs
        right_red_read_counter = right_red_read_counter + 1 #Update the counter
        time.sleep(0.01) #Small delay to catch next pulse
    kwh = right_red_read_counter / 1000.0 #Real time measuring kwh
    update_json_file(port_name, kwh)  #Update the value to the UI
    
     #Just for debugging purposes
    
    ##right_red_read_counter += 1
    ##kwh = right_red_read_counter / 1000.0
    ##update_json_file(port_name, kwh)

def read_left_blue(port_name):
    global left_blue_read_counter #Global counter that counts high pulses from metrics
    left_blue_value = GPIO.input(meter_3_input_pin) #Read input pin
    if left_blue_value == GPIO.HIGH: #If a high pulse occurs
        left_blue_read_counter = left_blue_read_counter + 1 #Update the counter
        time.sleep(0.01) #Small delay to catch next pulse
    kwh = left_blue_read_counter / 1000.0 #Real time measuring kwh   
    update_json_file(port_name, kwh)  #Update the value to the UI
    
     #Just for debugging purposes
    
    ##left_blue_read_counter += 1
    ##kwh = left_blue_read_counter / 1000.0
    ##update_json_file(port_name, kwh)

def read_right_blue(port_name):
    global right_blue_read_counter #Global counter that counts high pulses from metrics
    right_blue_value = GPIO.input(meter_4_input_pin) #Read input pin
    if right_blue_value == GPIO.HIGH: #If a high pulse occurs
        right_blue_read_counter = right_blue_read_counter + 1 #Update the counter
        time.sleep(0.01) #Small delay to catch next pulse
    kwh = right_blue_read_counter / 1000.0 #Real time measuring kwh    
    update_json_file(port_name, kwh)  #Update the value to the UI
    
     #Just for debugging purposes
    
    ##right_blue_read_counter += 1
    ##kwh = right_blue_read_counter / 1000.0
    ##update_json_file(port_name, kwh)

def read_water_1(port_name):
    global left_water_1_read_counter #Global counter that counts high pulses from metrics
    water_1_value = GPIO.input(water_1_input_pin) #Read input pin
    if water_1_value == GPIO.HIGH: #If a high pulse occurs
        left_water_1_read_counter = left_water_1_read_counter + 1 #Update the counter
        time.sleep(0.01) #Small delay to catch next pulse
    kwh = left_water_1_read_counter / 1000.0 #Real time measuring kwh        
    update_json_file(port_name, kwh)  #Update the value to the UI
    
     #Just for debugging purposes
    
    ##left_water_1_read_counter += 1
    ##kwh = left_water_1_read_counter / 1000.0
    ##update_json_file(port_name, kwh)

def read_water_2(port_name):
    global right_water_2_read_counter #Global counter that counts high pulses from metrics
    water_2_value = GPIO.input(water_2_input_pin) #Read input pin
    if water_2_value == GPIO.HIGH:
        right_water_2_read_counter = right_water_2_read_counter + 1 #Update the counter
        time.sleep(0.01) #Small delay to catch next pulse
    kwh = right_water_2_read_counter / 1000.0 #Real time measuring kwh       
    update_json_file(port_name, kwh)  #Update the value to the UI
    
     #Just for debugging purposes
    
    ##right_water_2_read_counter += 1
    ##kwh = right_water_2_read_counter / 1000.0
    ##update_json_file(port_name, kwh)

########	END OF READING OPERATIONS	########


########	FUNCTIONS THAT CLOSING SOCKETS	  ########
def close_left_red(port_name):
	global left_red_read_counter
	left_red_read_counter = 0 #Reset the value of port into the UI
	update_json_file(port_name,left_red_read_counter)
	#Notify user
	print("Left red socket 3-PH closed successfully")
	GPIO.output(priza_1_output_pin, GPIO.HIGH) #Open relay, in order to stop metric to send pulses

def close_right_red(port_name):
	global right_red_read_counter
	right_red_read_counter = 0 #Reset the value of port into the UI
	update_json_file(port_name,right_red_read_counter) #Update the UI with the reset value
	#Notify user
	print("Right red socket 3-PH closed successfully")
	GPIO.output(priza_2_output_pin, GPIO.HIGH) #Open relay, in order to stop metric to send pulses
	
def close_left_blue(port_name):
	global left_blue_read_counter 	
	left_blue_read_counter = 0 #Reset the value of port into the UI
	update_json_file(port_name,left_blue_read_counter) #Update the UI with the reset value
	#Notify user
	print("Left blue socket 1-PH closed successfully")
	GPIO.output(priza_3_output_pin, GPIO.HIGH) #Open relay, in order to stop metric to send pulses

def close_right_blue(port_name):
	global right_blue_read_counter
	right_blue_read_counter = 0 #Reset the value of port into the UI
	update_json_file(port_name,right_blue_read_counter) #Update the UI with the reset value
	#Notify user
	print("Right blue socket 1-PH closed successfully")
	GPIO.output(priza_4_output_pin, GPIO.HIGH) #Open relay, in order to stop metric to send pulses

def close_water_1(port_name):
	global left_water_1_read_counter
	left_water_1_read_counter = 0 #Reset the value of port into the UI
	update_json_file(port_name,left_water_1_read_counter) #Update the UI with the reset value
	#Notify user
	print("Water 1 socket closed successfully")
	GPIO.output(water_1_output_pin, GPIO.HIGH) #Open relay, in order to stop metric to send pulses
	
def close_water_2(port_name):
	global right_water_2_read_counter
	right_water_2_read_counter = 0 #Reset the value of port into the UI
	update_json_file(port_name,right_water_2_read_counter) #Update the UI with the reset value
	#Notify user
	print("Water 2 socket closed successfully")
	GPIO.output(water_2_output_pin, GPIO.HIGH) #Open relay, in order to stop metric to send pulses

########	END OF FUNCTIONS THAT CLOSING SOCKETS	########

#This function update the JSON file with the kwh value of every port that is open, in order to appear into the UI
def update_json_file(port, counter):
    
    with open('data.json', 'r+') as file: #Open JSON file for reading and writing
        data = json.load(file) #Load the contents of the file into the variable data

        data[port]["value"] = counter #Update the data of the file with the new value, into the item "value"

        file.seek(0) #Return the pointer to the starting point of the file

        json.dump(data, file) #Write the data back to the file
        
        file.truncate() #Stop ghosts if there is any


def main():
	
	gpio_init() #Initialize GPIO pins
	
	while True:
		try:
			with open('data.json','r') as file: #Read the file contents
				data = json.load(file) #Load the contents of the file into the variable data
			
			for port_name, settings in data.items():
				operation = settings['operasion'] #Take the operation value
				handle_ports(port_name, operation) #Enable handle port function, in order to open the port with the specific operation
			time.sleep(1) #Check the JSON file every one second
			
		except KeyboardInterrupt: #If there is any keyboard Interrupt 
			print("Keyboard interrupt occurd") #Notify user
			
	

if __name__ == "__main__":
    main()
